﻿namespace Default_PSN_Avatar
{
    partial class RemoveAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RemoveAddress));
            this.DelAddress = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BearerText = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // DelAddress
            // 
            this.DelAddress.Location = new System.Drawing.Point(12, 203);
            this.DelAddress.Name = "DelAddress";
            this.DelAddress.Size = new System.Drawing.Size(367, 47);
            this.DelAddress.TabIndex = 0;
            this.DelAddress.Text = "Remove Address Information";
            this.DelAddress.UseVisualStyleBackColor = true;
            this.DelAddress.Click += new System.EventHandler(this.DelAddress_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Default_PSN_Avatar.Properties.Resources.NoAddress;
            this.pictureBox1.Location = new System.Drawing.Point(12, 88);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(367, 83);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Created By: SilicaAndPina";
            // 
            // BearerText
            // 
            this.BearerText.AutoSize = true;
            this.BearerText.Location = new System.Drawing.Point(12, 9);
            this.BearerText.Name = "BearerText";
            this.BearerText.Size = new System.Drawing.Size(44, 13);
            this.BearerText.TabIndex = 3;
            this.BearerText.Text = "Bearer: ";
            // 
            // RemoveAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 262);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BearerText);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.DelAddress);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RemoveAddress";
            this.Text = "Remove Address";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RemoveAddress_FormClosing);
            this.Load += new System.EventHandler(this.RemoveAddress_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button DelAddress;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label BearerText;
    }
}