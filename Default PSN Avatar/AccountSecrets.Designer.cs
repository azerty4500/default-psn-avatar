﻿namespace Default_PSN_Avatar
{
    partial class AccountSecrets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AccountSecrets));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.accDoB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.accAID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.accUUID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.accCUUID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.accReigon = new System.Windows.Forms.TextBox();
            this.getEntitlements = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Created By: SilicaAndPina";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "DoB:";
            // 
            // accDoB
            // 
            this.accDoB.Location = new System.Drawing.Point(42, 50);
            this.accDoB.Name = "accDoB";
            this.accDoB.ReadOnly = true;
            this.accDoB.Size = new System.Drawing.Size(428, 20);
            this.accDoB.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "AID";
            // 
            // accAID
            // 
            this.accAID.Location = new System.Drawing.Point(42, 76);
            this.accAID.Name = "accAID";
            this.accAID.ReadOnly = true;
            this.accAID.Size = new System.Drawing.Size(428, 20);
            this.accAID.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(0, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "UUID:";
            // 
            // accUUID
            // 
            this.accUUID.Location = new System.Drawing.Point(42, 102);
            this.accUUID.Name = "accUUID";
            this.accUUID.ReadOnly = true;
            this.accUUID.Size = new System.Drawing.Size(428, 20);
            this.accUUID.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(0, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "CUUID:";
            // 
            // accCUUID
            // 
            this.accCUUID.Location = new System.Drawing.Point(42, 128);
            this.accCUUID.Name = "accCUUID";
            this.accCUUID.ReadOnly = true;
            this.accCUUID.Size = new System.Drawing.Size(428, 20);
            this.accCUUID.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(0, 157);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "REGION:";
            // 
            // accReigon
            // 
            this.accReigon.Location = new System.Drawing.Point(58, 154);
            this.accReigon.Name = "accReigon";
            this.accReigon.ReadOnly = true;
            this.accReigon.Size = new System.Drawing.Size(412, 20);
            this.accReigon.TabIndex = 14;
            // 
            // getEntitlements
            // 
            this.getEntitlements.Location = new System.Drawing.Point(9, 180);
            this.getEntitlements.Name = "getEntitlements";
            this.getEntitlements.Size = new System.Drawing.Size(461, 49);
            this.getEntitlements.TabIndex = 15;
            this.getEntitlements.Text = "Get Internal Entitlements JSON";
            this.getEntitlements.UseVisualStyleBackColor = true;
            this.getEntitlements.Click += new System.EventHandler(this.getEntitlements_Click);
            // 
            // AccountSecrets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 241);
            this.Controls.Add(this.getEntitlements);
            this.Controls.Add(this.accReigon);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.accCUUID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.accUUID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.accAID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.accDoB);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AccountSecrets";
            this.ShowInTaskbar = false;
            this.Text = "Account Secrets";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AccountSecrets_FormClosing);
            this.Load += new System.EventHandler(this.AccountSecrets_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox accDoB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox accAID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox accUUID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox accCUUID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox accReigon;
        private System.Windows.Forms.Button getEntitlements;
    }
}