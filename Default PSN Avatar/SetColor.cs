﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Default_PSN_Avatar
{
    public partial class SetColor : Form
    {
        string Bearer;
        public SetColor(string Token)
        {
            InitializeComponent();
            BearerText.Text = "Bearer: " + Token;
            Bearer = Token;
        }

        private string getRGBHex(Color col)
        {
            string RGBHex = "";
            RGBHex += col.R.ToString("X2");
            RGBHex += col.G.ToString("X2");
            RGBHex += col.B.ToString("X2");
            return RGBHex;
        }
        private void PickColor_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            colorDialog.ShowDialog();
            hexCode.Text = getRGBHex(colorDialog.Color);
        }

        private void hexCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int ColorCode = Int32.Parse(hexCode.Text, NumberStyles.HexNumber);

                if (ColorCode >= 0 && ColorCode <= 0xFFFFFF && hexCode.Text.Length == 6)
                {
                    colorDisplayBox.BackColor = ColorTranslator.FromHtml("#" + hexCode.Text);
                }

            }
            catch (Exception) { };

        }

        private void hexCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            string[] allowedKeys = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };

            if (allowedKeys.Contains(e.KeyChar.ToString().ToUpper()))
            {
                e.KeyChar = e.KeyChar.ToString().ToUpper()[0];
            }
            else if (e.KeyChar >= (char)0x20 && e.KeyChar <= (char)0x7E)
            {
                e.Handled = true;
            }
        }

        private void SetColor_FormClosing(object sender, FormClosingEventArgs e)
        {
            SelectOptions selOption = new SelectOptions(Bearer);
            selOption.Show();
            this.Hide();
        }

        private void setAsBgColor_Click(object sender, EventArgs e)
        {
            try
            {
                setAsBgColor.Enabled = false;
                var ChangeRequest = new System.Net.WebClient();
                ChangeRequest.Headers.Set("Content-Type", "application/json; charset=UTF-8-Type");
                ChangeRequest.Headers.Set("User-Agent", "PlayStation/19.10.0(Android)");
                ChangeRequest.Headers.Set("Authorization", "Bearer " + Bearer);

                string Response = ChangeRequest.UploadString("https://profile.api.playstation.com/v1/users/me/profile/backgroundImage", "PATCH", "{\"ops\":[{\"op\":\"replace\",\"path\":\"/color\",\"value\":\""+getRGBHex(colorDisplayBox.BackColor)+"\"}]}");
                if (Response == "")
                {
                    MessageBox.Show("Background Color Changed Successfully!", "SUCCESS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(Response, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                setAsBgColor.Enabled = true;
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                setAsBgColor.Enabled = false;
            }

        }
    }
}
