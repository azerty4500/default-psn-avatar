﻿using System;
using System.Net;
using System.Windows.Forms;

namespace Default_PSN_Avatar
{
    public partial class RemoveAddress : Form
    {
        String Bearer;
        public RemoveAddress(string Token)
        {
            Bearer = Token;
            InitializeComponent();
        }

        private void RemoveAddress_FormClosing(object sender, FormClosingEventArgs e)
        {
            SelectOptions selOption = new SelectOptions(Bearer);
            selOption.Show();
            this.Hide();
        }

        private void RemoveAddress_Load(object sender, EventArgs e)
        {
            BearerText.Text = "Bearer: " + Bearer;
        }

        private void DelAddress_Click(object sender, EventArgs e)
        {
            try
            {
                DelAddress.Enabled = false;

                var GetCurrent = new WebClient();

                GetCurrent.Headers.Set("Origin", "https://id.sonyentertainmentnetwork.com");
                GetCurrent.Headers.Set("Referer", "https://id.sonyentertainmentnetwork.com/id/management/");
                GetCurrent.Headers.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
                GetCurrent.Headers.Set("Authorization", "Bearer " + Bearer);

                string json = GetCurrent.DownloadString("https://accounts.api.playstation.com/api/v1/accounts/me/addresses");
                GetCurrent.Dispose();

                int Index = json.IndexOf("\"country\"");
                string OldSettings = json.Substring(Index,  json.Length - Index);

                int IdIndex = OldSettings.IndexOf("\"id\":\"") + "\"id\":\"".Length;
                string ID = OldSettings.Substring(IdIndex, OldSettings.Length - IdIndex);
                IdIndex = ID.IndexOf("\"}");
                ID = ID.Substring(0, IdIndex);

                string NewSettings = "{\"qualifier\":\"residence\",\"line1\":\"\",\"line2\":\"\",\"line3\":\"\",\"city\":\"\",\"postalCode\":\"\",";
                string FullJson = NewSettings + OldSettings.Substring(0,OldSettings.Length - 1);

                var RemoveRequest = new ExpectContinueAware();

                RemoveRequest.Headers.Set("Origin", "https://id.sonyentertainmentnetwork.com");
                RemoveRequest.Headers.Set("Referer", "https://id.sonyentertainmentnetwork.com/id/management/");
                RemoveRequest.Headers.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
                RemoveRequest.Headers.Set("Authorization", "Bearer " + Bearer);
                RemoveRequest.Headers.Set("Content-Type", "application/json; charset=UTF-8");


                string Response = RemoveRequest.UploadString("https://accounts.api.playstation.com/api/v1/accounts/me/addresses/"+ID, "PUT", FullJson);
                if (Response == "")
                {
                    MessageBox.Show("Address Information Removed Successfully.", "SUCCESS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(Response, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                GetCurrent.Dispose();
                DelAddress.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                DelAddress.Enabled = true;
            }
        }

    }
}
