﻿using System;
using System.Net;

namespace Default_PSN_Avatar
{
    public class ExpectContinueAware : WebClient
    {

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);
            if (request is HttpWebRequest)
            {
                var hwr = request as HttpWebRequest;
                hwr.ServicePoint.Expect100Continue = false;
                hwr.KeepAlive = true;
                hwr.AllowAutoRedirect = Redirects;
            }
            return request;
        }
        Uri _responseUri;
        public bool Redirects = true;

        public Uri ResponseUri
        {
            get { return _responseUri; }
        }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            _responseUri = response.ResponseUri;
            return response;
        }
    }
}
