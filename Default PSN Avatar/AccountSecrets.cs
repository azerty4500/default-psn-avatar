﻿using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Windows.Forms;

namespace Default_PSN_Avatar
{
    public partial class AccountSecrets : Form
    {

        private string bearer;
        public string pubBearer;
        public AccountSecrets(string Token)
        {
            pubBearer = Token;
            InitializeComponent();
        }

        private void AccountSecrets_Load(object sender, EventArgs e)
        {
            if(LoginFormCR.ssoCookie == null)
            {
                MessageBox.Show("No ssotoken avaible.", "Fail", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            // Request exta permissions ... 
            ExpectContinueAware wc = new ExpectContinueAware();
            wc.Headers.Add("Cookie", "npsso=" + LoginFormCR.ssoCookie);
            wc.Headers.Set("Origin", "https://id.sonyentertainmentnetwork.com");
            wc.Headers.Set("Referer", "https://id.sonyentertainmentnetwork.com/id/management/");
            wc.Headers.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");

            wc.DownloadString(@"https://ca.account.sony.com/api/v1/oauth/authorize?response_type=token&prompt=none&client_id=894cc20a-89f9-4e40-977c-76736871f7da&scope=kamaji%3Aget_account_hash%2Cuser%3Aaccount.address.get%2Cuser%3Aaccount.address.update%2Cuser%3Aaccount.core.get%2Cuser%3Aaccount.languages.get%2Cuser%3Aaccount.subaccounts.get%2Cversa%3Atv_get_dmas%2Cversa%3Auser_get_dma%2Cversa%3Auser_update_dma%2Cwallets%3Ainstrument.get%2Cwallets%3Ainstrument.verify%2Cwallets%3AmetaInfo.get%2C3DS%3AtransactionData.update&redirect_uri=https%3A%2F%2Ftransact.playstation.com%2Fhtml%2FwebIframeRedirect.html%3FrequestId%3D8901969a-9543-4950-acef-6a2e5a6fd458&ui=pr");

            string Url = wc.ResponseUri.AbsoluteUri;

            if (Url.Contains("access_token"))
            {
                int i = Url.IndexOf("access_token=") + 13;
                string Bearer = Url.Substring(i);
                i = Bearer.IndexOf("&");
                bearer = Bearer.Substring(0, i);
            }
            else
            {
                MessageBox.Show("Failed to get permission ...", "Fail", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            wc.Dispose();

            wc = new ExpectContinueAware();
            wc.Headers.Set("Origin", "https://id.sonyentertainmentnetwork.com");
            wc.Headers.Set("Referer", "https://id.sonyentertainmentnetwork.com/id/management/");
            wc.Headers.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
            wc.Headers.Set("Authorization", "Bearer " + bearer);
            string secrets = wc.DownloadString(@"https://accounts.api.playstation.com/api/v1/accounts/me/core");
            dynamic response = JsonConvert.DeserializeObject(secrets);
            string dateStr = response.dateOfBirth;
            DateTime DoB = DateTime.ParseExact(dateStr, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
            accDoB.Text = DoB.ToString("D");

            UInt64 aid = response.accountId;
            byte[] aidBuf = BitConverter.GetBytes(aid);
            accAID.Text = BitConverter.ToString(aidBuf).Replace("-","").ToLower();

            accUUID.Text = response.accountUuid;
            accCUUID.Text = response.correlationUuid;

            accReigon.Text = response.region + " (" + response.legalCountry + ")";

            wc.Dispose();
        }

        private void AccountSecrets_FormClosing(object sender, FormClosingEventArgs e)
        {
            SelectOptions selOption = new SelectOptions(pubBearer);
            selOption.Show();
            this.Hide();
        }

        private void getEntitlements_Click(object sender, EventArgs e)
        {
            string code = "";
            string access_token;
 
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Javascript Object Notation| *.json";
            sfd.FileName = "internal_entitlements.json";
            sfd.ShowDialog();
            try
            {
                ExpectContinueAware wc = new ExpectContinueAware();
                wc.Headers.Add("Cookie", "npsso=" + LoginFormCR.ssoCookie);
                wc.Headers.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
                wc.Redirects = false;

                wc.DownloadString(@"https://ca.account.sony.com/api/authz/v3/oauth/authorize?access_type=offline&client_id=ac8d161a-d966-4728-b0ea-ffec22f69edc&redirect_uri=com.playstation.PlayStationApp%3A%2F%2Fredirect&response_type=code&scope=psn%3Aclientapp%20psn%3Amobile.v1");

                string Url = wc.ResponseHeaders.Get("Location");

                if (Url.Contains("code"))
                {
                    int i = Url.IndexOf("?code=") + 6;
                    string Code = Url.Substring(i);
                    i = Code.IndexOf("&");
                    code = Code.Substring(0, i);
                }
                else
                {
                    MessageBox.Show("Failed to get permission ...", "Fail", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                wc.Dispose();

                wc = new ExpectContinueAware();
                wc.Headers.Set("Referer", "https://my.playstation.com/");
                wc.Headers.Set("Authorization", "Basic YWM4ZDE2MWEtZDk2Ni00NzI4LWIwZWEtZmZlYzIyZjY5ZWRjOkRFaXhFcVhYQ2RYZHdqMHY=");
                wc.Headers.Set("Content-Type", "application/x-www-form-urlencoded");

                string output = wc.UploadString(@"https://ca.account.sony.com/api/authz/v3/oauth/token?access_type=offline&client_id=ac8d161a-d966-4728-b0ea-ffec22f69edc&redirect_uri=com.playstation.PlayStationApp%3A%2F%2Fredirect&response_type=code&scope=psn%3Aclientapp%20psn%3Amobile.v1", "code=" + code + "&grant_type=authorization_code&redirect_uri=com.playstation.PlayStationApp%3A%2F%2Fredirect&scope=psn%3Aclientapp%20psn%3Amobile.v1&token_format=jwt");
                wc.Dispose();

                dynamic response = JsonConvert.DeserializeObject(output);
                access_token = response.access_token;

                wc = new ExpectContinueAware();
                wc.Headers.Set("Authorization", "Bearer " + access_token);
                wc.DownloadFile(@"https://commerce.api.np.km.playstation.net/commerce/api/v1/users/me/internal_entitlements?start=0&size=2147483647&fields=meta_rev%2Ccloud_meta%2Creward_meta%2Cgame_meta%2Cdrm_def%2Cdrm_def.content_type", sfd.FileName);
                MessageBox.Show("Saved JSON File.", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception except) { 
                MessageBox.Show(except.Message, "Done", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
        }
    }
}
