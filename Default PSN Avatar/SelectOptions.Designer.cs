﻿namespace Default_PSN_Avatar
{
    partial class SelectOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectOptions));
            this.BearerText = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DefaultAvatar = new System.Windows.Forms.Button();
            this.RemoveName = new System.Windows.Forms.Button();
            this.EndSession = new System.Windows.Forms.Button();
            this.customColor = new System.Windows.Forms.Button();
            this.removeAddress = new System.Windows.Forms.Button();
            this.getSecrets = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BearerText
            // 
            this.BearerText.AutoSize = true;
            this.BearerText.Location = new System.Drawing.Point(12, 9);
            this.BearerText.Name = "BearerText";
            this.BearerText.Size = new System.Drawing.Size(41, 13);
            this.BearerText.TabIndex = 0;
            this.BearerText.Text = "Bearer:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Option:";
            // 
            // DefaultAvatar
            // 
            this.DefaultAvatar.Location = new System.Drawing.Point(15, 60);
            this.DefaultAvatar.Name = "DefaultAvatar";
            this.DefaultAvatar.Size = new System.Drawing.Size(316, 28);
            this.DefaultAvatar.TabIndex = 2;
            this.DefaultAvatar.Text = "Set Default Avatar";
            this.DefaultAvatar.UseVisualStyleBackColor = true;
            this.DefaultAvatar.Click += new System.EventHandler(this.DefaultAvatar_Click);
            // 
            // RemoveName
            // 
            this.RemoveName.Location = new System.Drawing.Point(15, 94);
            this.RemoveName.Name = "RemoveName";
            this.RemoveName.Size = new System.Drawing.Size(316, 27);
            this.RemoveName.TabIndex = 3;
            this.RemoveName.Text = "Change \"Real Name\" Entry";
            this.RemoveName.UseVisualStyleBackColor = true;
            this.RemoveName.Click += new System.EventHandler(this.RemoveName_Click);
            // 
            // EndSession
            // 
            this.EndSession.Location = new System.Drawing.Point(12, 226);
            this.EndSession.Name = "EndSession";
            this.EndSession.Size = new System.Drawing.Size(316, 27);
            this.EndSession.TabIndex = 4;
            this.EndSession.Text = "End Session";
            this.EndSession.UseVisualStyleBackColor = true;
            this.EndSession.Click += new System.EventHandler(this.EndSession_Click);
            // 
            // customColor
            // 
            this.customColor.Location = new System.Drawing.Point(15, 160);
            this.customColor.Name = "customColor";
            this.customColor.Size = new System.Drawing.Size(316, 27);
            this.customColor.TabIndex = 5;
            this.customColor.Text = "Set Custom Profile Color";
            this.customColor.UseVisualStyleBackColor = true;
            this.customColor.Click += new System.EventHandler(this.customColor_Click);
            // 
            // removeAddress
            // 
            this.removeAddress.Location = new System.Drawing.Point(15, 127);
            this.removeAddress.Name = "removeAddress";
            this.removeAddress.Size = new System.Drawing.Size(316, 27);
            this.removeAddress.TabIndex = 6;
            this.removeAddress.Text = "Remove \"Address\" Entry";
            this.removeAddress.UseVisualStyleBackColor = true;
            this.removeAddress.Click += new System.EventHandler(this.removeAddress_Click);
            // 
            // getSecrets
            // 
            this.getSecrets.Location = new System.Drawing.Point(12, 193);
            this.getSecrets.Name = "getSecrets";
            this.getSecrets.Size = new System.Drawing.Size(316, 27);
            this.getSecrets.TabIndex = 7;
            this.getSecrets.Text = "View Account Secrets";
            this.getSecrets.UseVisualStyleBackColor = true;
            this.getSecrets.Click += new System.EventHandler(this.getSecrets_Click);
            // 
            // SelectOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 270);
            this.Controls.Add(this.getSecrets);
            this.Controls.Add(this.removeAddress);
            this.Controls.Add(this.customColor);
            this.Controls.Add(this.EndSession);
            this.Controls.Add(this.RemoveName);
            this.Controls.Add(this.DefaultAvatar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BearerText);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SelectOptions";
            this.Text = "Select Option";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SelectOptions_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label BearerText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button DefaultAvatar;
        private System.Windows.Forms.Button RemoveName;
        private System.Windows.Forms.Button EndSession;
        private System.Windows.Forms.Button customColor;
        private System.Windows.Forms.Button removeAddress;
        private System.Windows.Forms.Button getSecrets;
    }
}