﻿namespace Default_PSN_Avatar
{
    partial class SetAvatar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetAvatar));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BearerText = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SetAvi = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::Default_PSN_Avatar.Properties.Resources.DefaultAvatar;
            this.pictureBox1.Location = new System.Drawing.Point(50, 64);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(160, 169);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // BearerText
            // 
            this.BearerText.AutoSize = true;
            this.BearerText.Location = new System.Drawing.Point(12, 9);
            this.BearerText.Name = "BearerText";
            this.BearerText.Size = new System.Drawing.Size(44, 13);
            this.BearerText.TabIndex = 1;
            this.BearerText.Text = "Bearer: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Created By SilicaAndPina";
            // 
            // SetAvi
            // 
            this.SetAvi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.SetAvi.Location = new System.Drawing.Point(12, 253);
            this.SetAvi.Name = "SetAvi";
            this.SetAvi.Size = new System.Drawing.Size(243, 37);
            this.SetAvi.TabIndex = 4;
            this.SetAvi.Text = "Set Avatar";
            this.SetAvi.UseVisualStyleBackColor = true;
            this.SetAvi.Click += new System.EventHandler(this.SetAvi_Click);
            // 
            // SetAvatar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(267, 311);
            this.Controls.Add(this.SetAvi);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BearerText);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SetAvatar";
            this.Text = "Set Avatar";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SetAvatar_FormClosing);
            this.Load += new System.EventHandler(this.SetAvatar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label BearerText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button SetAvi;
    }
}