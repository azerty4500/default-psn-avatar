﻿using CefSharp;
using CefSharp.WinForms;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net;
using System.Windows.Forms;

namespace Default_PSN_Avatar
{
    public partial class LoginFormCR : Form
    {
        public ChromiumWebBrowser chromeBrowser;
        bool IsLoggingOut = false;
        bool IsClosing = false;
        bool GotSso = false;
        public static string ssoCookie = null;
        public static string bearer;
        public void LogoutOfPSN()
        {
            IsLoggingOut = true;
            IsUrlGoneTo = false;
            chromeBrowser.EvaluateScriptAsync("document.getElementsByClassName(\"row-unshrink signout-button\")[0].children[0].click()");
        }
        public void LogoutOfPSNAndExit()
        {
            IsClosing = true;
            LogoutOfPSN();
        }

        public void InitializeChromium(string ua)
        {
            IsUrlGoneTo = false;
            if (chromeBrowser != null)
            {
                chromeBrowser.GetDevToolsClient().Emulation.SetUserAgentOverrideAsync(ua);
                chromeBrowser.Load("https://id.sonyentertainmentnetwork.com/id/management/#/p/psn_profile/list?entry=psn_profile");
                return;
            }

            CefSettings settings = new CefSettings();
            settings.LogSeverity = LogSeverity.Disable;
            settings.UserAgent = ua;
            settings.PersistSessionCookies = false;
            settings.PersistUserPreferences = false;
            Cef.Initialize(settings);
            Cef.GetGlobalCookieManager().DeleteCookies("", "");
            chromeBrowser = new ChromiumWebBrowser("https://id.sonyentertainmentnetwork.com/id/management/#/p/psn_profile/list?entry=psn_profile");
            this.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;
            chromeBrowser.AddressChanged += ChromeBrowser_AddressChanged;
            chromeBrowser.LoadingStateChanged += ChromeBrowser_LoadingStateChanged;
        }

        private void ChromeBrowser_LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            if (e.IsLoading)
                return;

            if (chromeBrowser.Address.Contains("ssocookie"))
            {
                chromeBrowser.EvaluateScriptAsync("document.documentElement.innerText.toString()").ContinueWith(x =>
                {
                    GotSso = true;
                    chromeBrowser.Load("https://id.sonyentertainmentnetwork.com/id/management/#/p/psn_profile/list?entry=psn_profile");

                    string npsso = x.Result.Result.ToString();
                    dynamic response = JsonConvert.DeserializeObject(npsso);
                    if (response.npsso == null)
                    {
                        MessageBox.Show("Failed to get npsso :(", "NpSSO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        ssoCookie = response.npsso;
                    }
                });
            }
        }

        public LoginFormCR()
        {
            InitializeComponent();
        }
        public bool IsUrlGoneTo;

        private void ChromeBrowser_AddressChanged(object sender, AddressChangedEventArgs e)
        {

            if (IsLoggingOut)
            {
                if (chromeBrowser.Address.StartsWith("https://id.sonyentertainmentnetwork.com/signin"))
                {
                    IsLoggingOut = false;
                    if (IsClosing)
                    {
                        Invoke(new Action(() =>
                        {
                            chromeBrowser.Dispose();

                            try
                            {
                                Application.Exit();
                            }
                            catch (Exception) { };

                            Process.GetCurrentProcess().Kill(); //kys
                        }));

                    }
                    Invoke(new Action(() =>
                    {
                        Show();
                    }));
                }
            }

            if (IsUrlGoneTo == false)
            {
                if (chromeBrowser.Address.Contains("client_id"))
                {
                    int i = chromeBrowser.Address.IndexOf("&redirect_uri=");
                    string secondPartOfUrl = chromeBrowser.Address.Substring(i);
                    string firstPartOfUrl = "https://id.sonyentertainmentnetwork.com/signin/?ui=pr&response_type=token&scope=openid%3Auser_id%20openid%3Aonline_id%20openid%3Actry_code%20openid%3Alang%20user%3Aaccount.communication.get%20kamaji%3Aget_account_hash%20oauth%3Amanage_user_auth_sessions%20openid%3Aacct_uuid%20user%3Aaccount.authentication.mode.get%20user%3Aaccount.phone.masked.get%20user%3Aaccount.notification.create%20openid%3Acontent_ctrl%20user%3Aaccount.subaccounts.get%20kamaji%3Aget_internal_entitlements%20user%3AverifiedAccount.get%20kamaji%3Aaccount_link_user_link_account%20kamaji%3Aactivity_feed_set_feed_privacy%20user%3Aaccount.identityMapper%20user%3Aaccount.email.create%20user%3Aaccount.emailVerification.get%20user%3Aaccount.tosua.update%20user:account.communication.update.gated%20user:account.communication.update%20user:account.profile.get%20user:account.profile.update%20user:account.address.create.gated%20user:account.address.update.gated%20user:account.address.get.gated";
                    /*
                     * Add the following permissions to the signin page: 
                     * 
                     * user:account.communication.update.gated 
                     * user:account.communication.update
                     * user:account.profile.get
                     * user:account.profile.update
                     * user:account.address.create.gated
                     * user:account.address.update.gated
                     * user:account.address.get.gated
                     * 
                     * Needed for changing real name.
                     */
                    string NewUrl = firstPartOfUrl + secondPartOfUrl;
                    chromeBrowser.Load(NewUrl);
                    Console.WriteLine(NewUrl);
                    IsUrlGoneTo = true;


                }

            }

            
            if (chromeBrowser.Address.Contains("access_token"))
            {

                if (!GotSso)
                {
                    int i = chromeBrowser.Address.IndexOf("access_token=") + 13;
                    string Bearer = chromeBrowser.Address.Substring(i);
                    i = Bearer.IndexOf("&");
                    bearer = Bearer.Substring(0, i);

                    chromeBrowser.Load("https://auth.api.sonyentertainmentnetwork.com/2.0/ssocookie");

                    SelectOptions selOption = new SelectOptions(bearer);
                    selOption.Show();

                    Invoke(new Action(() =>
                    {
                        Hide();
                    }));

                }
            }
        }

        private void LoginFormCR_Load(object sender, EventArgs e)
        {
            // get latest chrome UA
            try
            {
                WebClient wc = new WebClient();
                string html = wc.DownloadString("https://www.whatismybrowser.com/guides/the-latest-user-agent/chrome");
                wc.Dispose();

                HtmlAgilityPack.HtmlDocument hdoc = new HtmlAgilityPack.HtmlDocument();
                hdoc.LoadHtml(html);
                userAgent.Text = hdoc.DocumentNode.SelectSingleNode("//*[@id=\"content-base\"]/section[2]/div/div[1]/table[1]/tbody/tr/td[2]/ul/li[1]").InnerText;
                
            }
            catch (Exception) 
            {
                userAgent.Text = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4430.93 Safari/537.36";
            }
            InitializeChromium(userAgent.Text);
        }

        private void userAgent_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                InitializeChromium(userAgent.Text);
            }
        }
    }
}
